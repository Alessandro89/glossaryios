//
//  SimpleTableCell.swift
//  Glossary
//
//  Created by Alessandro Novi on 04/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class SimpleTableCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var labelText: UILabel!
    
    var delegateDeleteRow:DelegateDeleteCustomRow?
    var idRow:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let imageViewDeleteBtn = UIImageView(image:deleteButton.currentImage!.withRenderingMode(.alwaysTemplate))
        imageViewDeleteBtn.tintColor = UIColor.orange
        deleteButton.setImage(imageViewDeleteBtn.image!, for: .normal)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteRow(_ sender: UIButton) {
        delegateDeleteRow!.deleteRow(idrow: idRow)
    }
}
