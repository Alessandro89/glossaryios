//
//  AddCategoryControllerTableViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 04/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class AddCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DelegateDeleteCustomRow {
    @IBOutlet weak var TableCategory: UITableView!

    var categories = [String]()
    var indexRow = 0
    let cellId = "simpleCell"
    var delegate: DelegateInsertWord?
    var modelGlossaryImpl = ModelGlossaryImpl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Glossary - Insert"
        self.tabBarItem.title = "Glossary - Insert"

        self.TableCategory.delegate = self
        self.TableCategory.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SimpleTableCell;
        let row = indexPath.row
        cell.labelText!.text = categories[row]
        cell.labelText!.textColor = UIColor.darkGray
        cell.textLabel?.font = cell.textLabel?.font.withSize(15.0)
        cell.idRow = indexPath.row
        cell.delegateDeleteRow = self
        return cell
    }
    
    @IBAction func ChooseCategory(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Scegli una categoria:",  message: "",preferredStyle: .actionSheet)
        
        var count = 0
        //visualizzo però solo le categorie mancanti!..
        for category in modelGlossaryImpl.getAllCategories(){
            if(!self.categories.contains(category)){
                count = count + 1
                alertController.addAction(UIAlertAction(title: category, style: UIAlertActionStyle.default, handler: {(action: UIAlertAction)->Void in
                    self.categories.append(category)
                    self.TableCategory.beginUpdates()
                    self.TableCategory.insertRows(at: [IndexPath(row: self.categories.count-1, section: 0)], with: .automatic)
                    self.TableCategory.endUpdates()
                    self.delegate?.addCategoryForMean(category: category, indexMean: self.indexRow)
                }))
            }
        }
        
        if(count == 0){
            alertController.message = "Le categorie di default sono state già aggiunte"
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
 
    @IBAction func AddCategory(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Inserisci una nuova categoria:",  message: "",preferredStyle: .alert)
        
        alertController.addTextField()

        
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {(action: UIAlertAction)->Void in
            let category = alertController.textFields![0].text!
            if(!category.isEmpty && !self.categories.contains(category)){
                self.categories.append(category)
                self.TableCategory.beginUpdates()
                self.TableCategory.insertRows(at: [IndexPath(row: self.categories.count-1, section:  0)], with: .automatic)
                self.TableCategory.endUpdates()
                self.delegate?.addCategoryForMean(category: category, indexMean: self.indexRow)
            }
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }

    func deleteRow(idrow: Int) {
        categories.remove(at: idrow)
        TableCategory.beginUpdates()
        let indPath = IndexPath(row: idrow, section: 0)
        TableCategory.deleteRows(at: [indPath], with: UITableViewRowAnimation.fade)
        TableCategory.endUpdates()
        self.delegate?.removeCategoryForMean(indexMean: self.indexRow, indexCategory: idrow)
        TableCategory.reloadData()
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
