//
//  TabBarController.swift
//  Glossary
//
//  Created by Alessandro Novi on 02/04/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.tabBarController?.tabBarController?.view.tintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor.white
        
        
        //UINavigationBar.appearance().backgroundColor = UITabBar.appearance().backgroundColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor =  UIColor.init(red: 72/255, green: 149/255, blue: 136/255, alpha: 1.0)
        //https://coderwall.com/p/dyqrfa/customize-navigation-bar-appearance-with-swift
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
