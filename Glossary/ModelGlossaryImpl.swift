//
//  ModelGlossaryImpl.swift
//  Glossary
//
//  Created by Alessandro Novi on 01/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import CoreData

class ModelGlossaryImpl{
    
    init(){
    }
    
    func deleteAllDataForTest(confirm:String){
        if(confirm == "yes"){
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext

            let fetchWords = NSFetchRequest<NSFetchRequestResult>(entityName: "Word")
            let fetchMeans = NSFetchRequest<NSFetchRequestResult>(entityName: "Mean")
            let fetchCategories = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
            let fetchExamples = NSFetchRequest<NSFetchRequestResult>(entityName: "Example")

            if let result = try? managedContext.fetch(fetchWords) as! [NSManagedObject]{
                for object in result {
                    managedContext.delete(object)
                }
            }
            if let result = try? managedContext.fetch(fetchMeans) as! [NSManagedObject]{
                for object in result {
                    managedContext.delete(object)
                }
            }
            if let result = try? managedContext.fetch(fetchCategories) as! [NSManagedObject]{
                for object in result {
                    managedContext.delete(object)
                }
            }
            if let result = try? managedContext.fetch(fetchExamples) as! [NSManagedObject]{
                for object in result {
                    managedContext.delete(object)
                }
            }
            try? managedContext.save()
            
            //cancelliamo il contenuto della cartella documents
            let urlsDirectoryDocument = try FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let urlDirectoryDocument = urlsDirectoryDocument[0]
            if let filePaths = try? FileManager.default.contentsOfDirectory(atPath: urlDirectoryDocument.path){
                for fileStr in filePaths{
                    let fullPath = urlDirectoryDocument.appendingPathComponent(fileStr)
                    print(fullPath)
                    let success = try? FileManager.default.removeItem(at: fullPath)
                    //print(success)
                }
            }
        }
    }
    
    func insertWord(wordModel:WordModel) throws {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.mergePolicy = NSMergePolicy.error
       
        
        //WORD
        let word = NSEntityDescription.insertNewObject(forEntityName: "Word", into: managedContext)
        
        word.setValue(wordModel.word, forKey: "name")
        word.setValue(1, forKey: "idtranslation")
        word.setValue(wordModel.dateOfWord, forKey: "date")
        if(wordModel.pathSoundBookMark != nil){
            word.setValue(wordModel.pathSoundBookMark!, forKey: "pathsoundbookmark")
        }
        
        //to do refactoring!..
        //MEANS:
        var managedMeans = [NSManagedObject]()
        for meanModel in wordModel.means{
            let mean = NSEntityDescription.insertNewObject(forEntityName: "Mean", into: managedContext)
            mean.setValue(meanModel.mean, forKey: "mean")
            managedMeans.append(mean)
            
            //EXAMPLES:
            var managedExamples = [NSManagedObject]()
            for exampleStr in meanModel.examples{
                let example = NSEntityDescription.insertNewObject(forEntityName: "Example", into: managedContext)
                example.setValue(exampleStr, forKey: "example")
                managedExamples.append(example)
            }
            
            mean.setValue(NSSet(array: managedExamples), forKey: "exampleRelation")

            
            //CATEGORY:
            var dictionaryCategory = getAllDictionaryCategory()
            
            var managedCategories = [NSManagedObject]()
            for categoryStr in meanModel.categories{
                if let category = dictionaryCategory[categoryStr]{//la categoria esiste già
                    managedCategories.append(category) //prendo quella esistente per fare la relazione!
                }
                else{
                    let category = NSEntityDescription.insertNewObject(forEntityName: "Category", into: managedContext)
                    category.setValue(categoryStr, forKey: "category")
                    managedCategories.append(category) //prendo quella che andrò a inserire per fare la relazione!
                }
            }
            
            mean.setValue(NSSet(array: managedCategories), forKey: "categoryRelation")

        }
        
        //create relationship..
        word.setValue(NSSet(array: managedMeans), forKey: "meansRelation")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            managedContext.rollback()
            throw error
        }
        
    }
    
    
    
    func getAllWords() -> [WordModel]{
        var arrayWords = [WordModel]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return arrayWords
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchWords = NSFetchRequest<NSFetchRequestResult>(entityName: "Word")
        let sortDescription = NSSortDescriptor(key: "date", ascending: false)
        fetchWords.sortDescriptors = [sortDescription]
        
        
        do {
            let wordsDb = try managedContext.fetch(fetchWords)
            for word in wordsDb{
                let wordStr:String = ((word as! NSObject).value(forKey: "name") as? String)!
                //let date:Date = ((word as! NSObject).value(forKey: "date") as? Date)!
                //let idTranslation = 1
                 let pathSoundBookMark:Data? = ((word as! NSObject).value(forKey: "pathsoundbookmark") as! Data?)
                let meansTry = ((word as! NSObject).value(forKey: "meansRelation") as? NSSet)!
                var meansArray = [MeanModel]()
                for meanObj in meansTry{
                    let meanStr:String = ((meanObj as! NSObject).value(forKey: "mean") as? String)!
                    var meanModel = MeanModel()
                    meanModel.mean = meanStr
                    
                    let examples = ((meanObj as! NSObject).value(forKey: "exampleRelation") as? NSSet)!
                    
                    var exampleArray = [String]()
                    for examplesObj in examples {
                        let examplesStr = ((examplesObj as! NSObject).value(forKey: "example") as? String)!
                        exampleArray.append(examplesStr)
                    }
                    
                    meanModel.examples = exampleArray
                    
                    let categories = ((meanObj as! NSObject).value(forKey: "categoryRelation") as? NSSet)!
                    
                    var categoryArray = [String]()
                    for categoryObj in categories {
                        let categoryStr = ((categoryObj as! NSObject).value(forKey: "category") as? String)!
                        categoryArray.append(categoryStr)
                    }
                    
                    meanModel.categories = categoryArray
                    meansArray.append(meanModel)
                    
                }
               
                arrayWords.append(WordModel(word: wordStr, means: meansArray, pathSoundBookMark: pathSoundBookMark))
                
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        return arrayWords
    }
    
    func getWord(name:String) -> WordModel? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchWords = NSFetchRequest<NSFetchRequestResult>(entityName: "Word")
        
        fetchWords.predicate = NSPredicate(format: "name == %@", name)
        
        do {
            let wordsDb = try managedContext.fetch(fetchWords)
            //ne restituirà solo uno..
            for wordDb in wordsDb{
                let wordStr:String = ((wordDb as! NSObject).value(forKey: "name") as? String)!
                //forse da problemi questo
                //let pathSound:String? = "inventiamo"
                
                let pathSoundBookMark:Data? = ((wordDb as! NSObject).value(forKey: "pathsoundbookmark") as! Data?)

                //let date:Date = ((word as! NSObject).value(forKey: "date") as? Date)!
                //let idTranslation = 1
                
                let meansTry = ((wordDb as! NSObject).value(forKey: "meansRelation") as? NSSet)!
                var meansArray = [MeanModel]()
                for meanObj in meansTry{
                    let meanStr:String = ((meanObj as! NSObject).value(forKey: "mean") as? String)!
                    var meanModel = MeanModel()
                    meanModel.mean = meanStr
                    
                    let examples = ((meanObj as! NSObject).value(forKey: "exampleRelation") as? NSSet)!
                    
                    var exampleArray = [String]()
                    for examplesObj in examples {
                        let examplesStr = ((examplesObj as! NSObject).value(forKey: "example") as? String)!
                        exampleArray.append(examplesStr)
                    }
                    
                    meanModel.examples = exampleArray
                    
                    let categories = ((meanObj as! NSObject).value(forKey: "categoryRelation") as? NSSet)!
                    
                    var categoryArray = [String]()
                    for categoryObj in categories {
                        let categoryStr = ((categoryObj as! NSObject).value(forKey: "category") as? String)!
                        categoryArray.append(categoryStr)
                    }
                    
                    meanModel.categories = categoryArray
                    meansArray.append(meanModel)
                    
                }
            
                return WordModel(word: wordStr, means: meansArray, pathSoundBookMark: pathSoundBookMark)
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        return nil
    }

    
    private func getAllDictionaryCategory() -> [String:NSManagedObject]{
        var dictionaryCategory = [String:NSManagedObject]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return dictionaryCategory
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchCategory = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        /*fetchCategory.entity = NSEntityDescription.entity(forEntityName: "Category", in: managedContext)*/
        
        do {
            let categoriesDb = try managedContext.fetch(fetchCategory) as! [NSManagedObject]
        
            for category in categoriesDb{
                let categoryStr:String = category.value(forKey: "category") as! String
                dictionaryCategory[categoryStr] = category
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        return dictionaryCategory
    }
    
    func getAllCategories() -> [String]{
        var arrayCategory = [String]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return arrayCategory
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchCategory = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        do {
            let categoriesDb = try managedContext.fetch(fetchCategory) as! [NSManagedObject]
            
            for category in categoriesDb{
                let categoryStr:String = category.value(forKey: "category") as! String
                arrayCategory.append(categoryStr)
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        return arrayCategory
    }
    
    func getWordsByCategories(categories:[String]) -> [WordModel]{
        var arrayWordModel = [WordModel]()
        var wordsName = [String]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return arrayWordModel
        }
        let managedContext = appDelegate.persistentContainer.viewContext

        
        let fetchWord = NSFetchRequest<NSFetchRequestResult>(entityName: "Word")
        fetchWord.predicate = NSPredicate(format: "SUBQUERY(meansRelation, $m, ANY $m.categoryRelation.category IN %@).@count > 0", categories);

        do {
            let wordsDb = try managedContext.fetch(fetchWord) as! [NSManagedObject]
            print(wordsDb.count)
            for word in wordsDb {
                arrayWordModel.append(getWordModel(wordDb: word))
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        
        return arrayWordModel
    }

    private func getWordModel(wordDb:Any) -> WordModel {
        let wordStr:String = ((wordDb as! NSObject).value(forKey: "name") as? String)!
        //let date:Date = ((word as! NSObject).value(forKey: "date") as? Date)!
        //let idTranslation = 1
        let pathSoundBookMark:Data? = ((wordDb as! NSObject).value(forKey: "pathsoundbookmark") as! Data?)

        
        let meansTry = ((wordDb as! NSObject).value(forKey: "meansRelation") as? NSSet)!
        var meansArray = [MeanModel]()
        for meanObj in meansTry{
            let meanStr:String = ((meanObj as! NSObject).value(forKey: "mean") as? String)!
            var meanModel = MeanModel()
            meanModel.mean = meanStr
    
            let examples = ((meanObj as! NSObject).value(forKey: "exampleRelation") as? NSSet)!
    
            var exampleArray = [String]()
            for examplesObj in examples {
                let examplesStr = ((examplesObj as! NSObject).value(forKey: "example") as? String)!
                exampleArray.append(examplesStr)
            }
    
            meanModel.examples = exampleArray
    
            let categories = ((meanObj as! NSObject).value(forKey: "categoryRelation") as? NSSet)!
    
            var categoryArray = [String]()
            for categoryObj in categories {
                let categoryStr = ((categoryObj as! NSObject).value(forKey: "category") as? String)!
                categoryArray.append(categoryStr)
            }
    
            meanModel.categories = categoryArray
            meansArray.append(meanModel)
    
        }
        return WordModel(word: wordStr, means: meansArray, pathSoundBookMark: pathSoundBookMark)

    }
    
    

}
