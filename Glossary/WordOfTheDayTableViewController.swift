//
//  WordOfTheDayTableViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 28/01/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class WordOfTheDayTableViewController: UITableViewController, DelegatePlayAudioCustomRow {

    let cellId = "simpleCellAudio"
    var SegueForMeans = "ShowMeans"
    var wordArray = [WordModel]()
    var modelGlossaryImpl = ModelGlossaryImpl()
    var managerAudios = [ManagerAudio]()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Glossary"
        
        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton

    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wordArray = modelGlossaryImpl.getAllWords()
        self.tableView.reloadData()
        
        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wordArray.count

    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SimpleTableCellAudio
        cell.labelWord.text = wordArray[indexPath.row].word
        cell.labelWord!.textColor = UIColor.darkGray
        cell.labelWord!.font = cell.textLabel?.font.withSize(15.0)
        cell.idRow = indexPath.row
        cell.delegatePlayWord = self
        cell.playButton.isHidden = true
        cell.playButton.imageView!.tintColor = UIColor.black
        if(wordArray[indexPath.row].pathSoundBookMark != nil){
            var isStale = false
            try? cell.urlSound = URL(resolvingBookmarkData: wordArray[indexPath.row].pathSoundBookMark!, bookmarkDataIsStale: &isStale)
        }
        cell.accessoryType = .disclosureIndicator
        print(managerAudios.count)
        return cell
    }
    
    // MARK: - Navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Perform Segue
        performSegue(withIdentifier: SegueForMeans, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueForMeans {
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! ShowMeanTableViewController
                destinationViewController.listMean = wordArray[indexPath.row].means
                destinationViewController.word = wordArray[indexPath.row].word

            }
        }
    }
 
    func errorsPlayRow(error: String) {
        showActionSheet(message: "error playing", title: "")
    }
    
    func showActionSheet(message:String,title:String){
        let alertController = UIAlertController(title: title,  message: message,preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func saveAudioManagerForPlaying(currentPlaying: ManagerAudio) {
        self.managerAudios.append(currentPlaying)
    }
    
    func removeAudioManagerForStopPlaying(currentStopPlaying: ManagerAudio) {
        if let indexAudio = self.managerAudios.index(of: currentStopPlaying){
            self.managerAudios.remove(at: indexAudio)
        }
    }
    
 


}
