//
//  ShowMeanTableViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 01/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class ShowMeanTableViewController: UITableViewController {

    let cellId = "idCell"
    var SegueCategoryExample = "ShowCategoryExample"

    @IBOutlet weak var labelMeans: UILabel!
    
    var word = ""
    var listMean = [MeanModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: cellId)
        self.labelMeans.text = "Means of " + word
        self.navigationItem.title = "Glossary"
        //self.navigationItem.
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        //http://stackoverflow.com/questions/24713505/backbarbuttonitem-in-ios-swift
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listMean.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath);
        
        let row = indexPath.row
        cell.textLabel?.text = listMean[row].mean
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = cell.textLabel?.font.withSize(14.0)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 3
        return cell
        
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Perform Segue
        performSegue(withIdentifier: SegueCategoryExample, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueCategoryExample {
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! PageViewControllerShowCategoryExample
                destinationViewController.categories = listMean[indexPath.row].categories
                destinationViewController.examples = listMean[indexPath.row].examples
            }
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
