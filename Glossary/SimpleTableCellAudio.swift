//
//  SimpleTableCellAudio.swift
//  Glossary
//
//  Created by Alessandro Novi on 21/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import AVFoundation

class SimpleTableCellAudio: UITableViewCell, DelegateListenerAudio {
    
    @IBOutlet weak var labelWord: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    
    var delegatePlayWord:DelegatePlayAudioCustomRow?
    var idRow:Int = 0
    var urlSound: URL? {
        didSet {
            if(urlSound != nil){
                playButton.isHidden = false
                self.audioManager = ManagerAudio(isRecord:false,urlAudio: urlSound, delegateListener: self)
            }
        }
    }
    var audioManager:ManagerAudio?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code 
        let imageViewPlayBtn = UIImageView(image:playButton.currentImage!.withRenderingMode(.alwaysTemplate))
        playButton.setImage(imageViewPlayBtn.image!, for: .normal)
        playButton.isHidden = true //all'inizio è invisibile
    }
    

    
    @IBAction func playSound(_ sender: Any) {
        do{
            try audioManager!.play()
        }catch let error{
            delegatePlayWord?.errorsPlayRow(error: "error playing")
            print(error)
        }
    }
    
    
    func onPlay() {
        playButton.imageView!.tintColor = UIColor.green
        delegatePlayWord?.saveAudioManagerForPlaying(currentPlaying:audioManager!)
    }
    func onStopPlay() {
        playButton.imageView!.tintColor = UIColor.black
        delegatePlayWord?.removeAudioManagerForStopPlaying(currentStopPlaying: audioManager!)
    }
}
