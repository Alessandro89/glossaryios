//
//  PageViewControllerShowCategoryExample.swift
//  Glossary
//
//  Created by Alessandro Novi on 07/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class PageViewControllerShowCategoryExample: UIPageViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    
    var pages = [UIViewController]()
    var examples = [String]()
    var categories = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        self.view.backgroundColor = UIColor.lightGray
        let showExampleController = storyboard?.instantiateViewController(withIdentifier: "ShowExampleController") as! ShowExampleViewController
        let showCategoryController = storyboard?.instantiateViewController(withIdentifier: "ShowCategoryController") as! ShowCategoryViewController
        
        pages.append(showExampleController)
        pages.append(showCategoryController)
        
        showExampleController.listExample = examples
        showCategoryController.categories = categories
        setViewControllers([showExampleController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.navigationItem.title = "Glossary"
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        //http://stackoverflow.com/questions/24713505/backbarbuttonitem-in-ios-swift
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        //credo che c'è un colore di sottofondo da togliere.. (con white tolgo il colore di sottofondo del page controller... e fare così la trasparenza) ma dovrei riuscire a togliere il colore anche del tab bar controller
        self.view.backgroundColor = UIColor.white
        
        //self.tabBarController?.tabBarController?.view.backgroundColor = UIColor.white
        //non funziona...
        
        UIPageControl.appearance().backgroundColor = UIColor.init(red: 90/255, green: 160/255, blue: 220/255, alpha: 0.7)
        
        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
       
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        let previousIndex = abs((currentIndex - 1) % pages.count)
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
