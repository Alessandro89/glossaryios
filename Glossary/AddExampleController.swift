//
//  AddMeans.swift
//  Glossary
//
//  Created by Alessandro Novi on 28/01/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import SystemConfiguration

class AddExampleController: UIViewController, UITableViewDataSource, UITableViewDelegate, DelegateDeleteCustomRow, DelegateDictionaryAdvice, DelegatePicker {
    
    
    @IBOutlet weak var buttonAdvice: UIButton!
    @IBOutlet weak var TableExample: UITableView!

    var examples = [String]()
    var indexRow = 0
    let cellId = "simpleCell"
    var word = ""
   
    var delegate: DelegateInsertWord?
    var seguePicker = "showPicker"
    var tempListExampleAdvice = [String]()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableExample.delegate = self
        self.TableExample.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return examples.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    @IBAction func AddExample(_ sender: UIButton) {
        //var text = MeanEditText.text!;
        let alertController = UIAlertController(title: "Inserisci un esempio:",  message: "",preferredStyle: .alert)
        
        alertController.addTextField()
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction)->Void in
            let example = alertController.textFields![0].text!
            
            if(!example.isEmpty && !self.examples.contains(example)){
                self.insertExample(example: example)
            }

        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func insertExample(example:String){
        self.examples.append(example)
        self.TableExample.beginUpdates()
        self.TableExample.insertRows(at: [IndexPath(row: self.examples.count-1, section: 0)], with: .automatic)
        self.TableExample.endUpdates()
        self.delegate?.addExampleForMean(example: example, indexMean: self.indexRow)
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCellEditingStyle,
                   forRowAt indexPath: IndexPath){
        //delete with swipe (scorrere premuto verso sinistra)
        if(editingStyle==UITableViewCellEditingStyle.delete){
            examples.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            tableView.endUpdates()
            self.delegate?.removeExampleForMean(indexMean: self.indexRow, indexExample: indexPath.row)
            
        }
        
    }
    
    @IBAction func showAdviceExample(_ sender: Any) {
        if(!isInternetAvailable()){
            print("the internet connection appears to be offline")
        }
        else{
            self.buttonAdvice.isEnabled = false //lo riabiliteremo quando ha scaricato..
            let dictAdvice = DictionaryAdvice(word: self.word,delegateDictionary: self)
            dictAdvice.getlistExample()
        }
        

    }

    
    func showListAdviceExample(adviceExample: [String], error: Error?) {
        self.buttonAdvice.isEnabled = true
        if(error != nil){
            let alertController = UIAlertController(title: error!.localizedDescription,  message: "",preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else if(adviceExample.count==0){
            let alertController = UIAlertController(title: "",  message: "",preferredStyle: .actionSheet)
            alertController.message = "nessun esempio trovato"
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            //tutto ok
            tempListExampleAdvice = adviceExample
            performSegue(withIdentifier: seguePicker, sender: self)
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == seguePicker {
            let destinationViewController = segue.destination as! PickerViewController
            destinationViewController.pickerDataSource = tempListExampleAdvice
            destinationViewController.mytitle = "Scegli un esempio:"
            destinationViewController.delegatePresentingController = self
        }
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SimpleTableCell;
        let row = indexPath.row
        cell.labelText?.text = examples[row]
        cell.idRow = indexPath.row
        cell.delegateDeleteRow = self
        return cell

    }
    
    func deleteRow(idrow: Int) {
        examples.remove(at: idrow)
        TableExample.beginUpdates()
        let indPath = IndexPath(row: idrow, section: 0)
        TableExample.deleteRows(at: [indPath], with: UITableViewRowAnimation.fade)
        TableExample.endUpdates()
        self.delegate?.removeExampleForMean(indexMean: self.indexRow, indexExample: idrow)
        TableExample.reloadData() //devo rifreshare gli id
    }

    func setChoice(value: String) {
        insertExample(example: value)

    }

    //http://stackoverflow.com/questions/39558868/check-internet-connection-ios-10
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }

    
}
