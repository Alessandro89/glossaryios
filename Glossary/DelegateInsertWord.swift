//
//  DelegateInsertWord.swift
//  Glossary
//
//  Created by Alessandro Novi on 01/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

protocol DelegateInsertWord{
    func addExampleForMean(example: String, indexMean: Int)
    func removeExampleForMean(indexMean: Int, indexExample: Int)

    func addCategoryForMean(category: String, indexMean: Int)
    func removeCategoryForMean(indexMean: Int, indexCategory: Int)

}
