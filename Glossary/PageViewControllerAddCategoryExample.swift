//
//  PageViewControllerAddCategoryExample.swift
//  Glossary
//
//  Created by Alessandro Novi on 03/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class PageViewControllerAddCategoryExample: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var pages = [UIViewController]()

    var examples = [String]()
    var categories = [String]()
    var word = ""
    var indexRow = 0
    var delegateWord: DelegateInsertWord?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        self.view.backgroundColor = UIColor.lightGray
        let addExampleController = storyboard?.instantiateViewController(withIdentifier: "AddExampleController") as! AddExampleController
        let addCategoryController = storyboard?.instantiateViewController(withIdentifier: "AddCategoryController") as! AddCategoryViewController
        
        pages.append(addExampleController)
        pages.append(addCategoryController)
        
        addExampleController.indexRow = indexRow
        addExampleController.examples = examples
        addExampleController.delegate = delegateWord
        addExampleController.word = word
        
        addCategoryController.indexRow = indexRow
        addCategoryController.categories = categories
        addCategoryController.delegate = delegateWord
        setViewControllers([addExampleController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.navigationItem.title = "Glossary"
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        //http://stackoverflow.com/questions/24713505/backbarbuttonitem-in-ios-swift
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        self.view.backgroundColor = UIColor.white
        
        //eng -it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
        
        //page control
        UIPageControl.appearance().backgroundColor = UIColor.init(red: 90/255, green: 160/255, blue: 220/255, alpha: 0.7)
        
        

    }

    func pageViewController(_ pageViewController: UIPageViewController,viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //do nothing.. return..
        //return nil
        //non ho messo transition style page curl per 2 problemi: uno è l'autolayout che salta via..
        //il secondo è la gestione del click da cambiare in swipe (cambia facilmente pagina)
        let currentIndex = pages.index(of: viewController)!
        let nextIndex = abs((currentIndex - 1) % pages.count)
        return pages[nextIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
