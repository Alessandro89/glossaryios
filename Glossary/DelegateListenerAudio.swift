//
//  DelegateUpdateViewAudio.swift
//  Glossary
//
//  Created by Alessandro Novi on 21/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

/*
tramite questi eventi puoi decidere di implementare il comportamento opportuno, come aggiornare la tua view
e altro
*/
@objc protocol DelegateListenerAudio{
    @objc optional func onRecord()
    @objc optional func onStopRecord()
    @objc optional func onPlay()
    @objc optional func onStopPlay()
    @objc optional func onErrors(error: String)
}
