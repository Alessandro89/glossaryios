//
//  InsertWordControllerViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 23/01/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import SystemConfiguration
import AVFoundation

class InsertWordViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DelegateInsertWord, DelegateDictionaryAdvice, DelegatePicker,DelegateListenerAudio, UITextFieldDelegate {
    @IBOutlet weak var WordEditText: UITextField!
    
    @IBOutlet weak var buttonAdvice: UIButton!
    
    @IBOutlet weak var TableMeans: UITableView!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var RecordButton: UIButton!
    @IBOutlet weak var PlayButton: UIButton!
    var audioManager:ManagerAudio?
    var urlAudioTemp:URL?
    
    var means = [MeanModel]()
    let textCellIdentifier = "TextCell"
    var SegueForAddMeans = "ShowAddExampleCategory"
    var modelGlossaryImpl = ModelGlossaryImpl()
    var seguePicker = "showPicker"
    var tempListMeanAdvice = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        inizializationForSound()

        WordEditText.delegate = self
        //modelGlossaryImpl.deleteAllDataForTest(confirm: "yes")
        TableMeans.delegate = self
        TableMeans.dataSource = self
        TableMeans.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: textCellIdentifier)
       
        self.navigationItem.title = "Glossary"
        
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
        // Do any additional setup after loading the view.
        
        //Looks for single or multiple taps.
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        //tap.delegate = self
        //view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.audioManager = ManagerAudio(isRecord:true,urlAudio: urlAudioTemp!, delegateListener: self)


    }

    
    @IBAction func AddMean(_ sender: UIButton) {

        let alertController = UIAlertController(title: "Inserisci un significato:",  message: "",preferredStyle: .alert)
        
        alertController.addTextField()
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction)->Void in
            let meanStr = alertController.textFields![0].text!
            
            if(!meanStr.isEmpty){
                let mean = MeanModel()
                mean.mean = meanStr
                self.means.append(mean)
                self.TableMeans.beginUpdates()
                self.TableMeans.insertRows(at: [IndexPath(row: self.means.count-1, section: 0)], with: .automatic)
                self.TableMeans.endUpdates()
            }

        }))

        self.present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func getAdviceMeans(_ sender: Any) {
        if(!isInternetAvailable()){
             showActionSheet(message: "la tua connessione internet dovrebbe essere offline", title: "Errore")
        }
        else{
            self.buttonAdvice.isEnabled = false //lo riabiliteremo quando ha scaricato..
            let dictAdvice = DictionaryAdvice(word: self.WordEditText!.text!,delegateDictionary: self)
            dictAdvice.getlistMeans()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return means.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath);
        
        let row = indexPath.row
        cell.textLabel?.text = means[row].mean
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = cell.textLabel?.font.withSize(14.0)
        cell.textLabel?.textColor = UIColor.darkGray
        //cell.textLabel?.font = UIFont(descriptor: .withFamily("Helvetica"), size: 10.0)
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 3
        //aCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
        //aCell.textLabel.lineBreakMode = NSLineBreakByWordWrapping; // Pre-iOS6 use UILineBreakModeWordWrap
        //aCell.textLabel.numberOfLines = 2;  // 0 means no max.
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCellEditingStyle,
                   forRowAt indexPath: IndexPath){
        //delete with swipe (scorrere premuto verso sinistra)
        if(editingStyle==UITableViewCellEditingStyle.delete){
            means.remove(at: indexPath.row)
            TableMeans.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            TableMeans.endUpdates()

        }

    }

    @IBAction func insertWord(_ sender: UIButton) {
      
        
        let wordStr = WordEditText.text!
        if(wordStr.isEmpty){
         //alert:
            let alertController = UIAlertController(title: "Nota", message:
            "Devi inserire una parola da tradurre con i suoi significati", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
         
            self.present(alertController, animated: true, completion: nil)
         }
         else{
            let word = WordModel(word: WordEditText.text!,means: means,pathSoundBookMark: nil)
            var urlSound:URL?
            do{
                let fileManager = FileManager.default
                //se esiste il file audio temp:
                if(fileManager.fileExists(atPath: self.urlAudioTemp!.path)){
                    //se esiste allora.. impostiamo il path di questo sound
                    do{
                        let path = try FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                        urlSound = path[0].appendingPathComponent(word.word)
                        //copiamo il file da temp a documents,lo facciamo prima per il bookmark
                        //se il file in documents esiste già, vuol dire che la parola esisterà già..e l'inserimento della parola non andrà a buon fine
                        if(!fileManager.fileExists(atPath: urlSound!.path)){ //se non esiste
                            do{
                                try fileManager.copyItem(atPath: urlAudioTemp!.path, toPath: urlSound!.path)
                            }catch let error{
                                showActionSheet(message: "errori nel salvataggio del suono", title: "Errore")
                                print(error)
                            }
                        }
                        else{
                            throw SoundError.duplicateSound(message: "il file audio della parola è già presente,controlla se la parola esiste già")
                        }

                        try word.pathSoundBookMark = urlSound!.bookmarkData() as Data?
                    }catch let SoundError.duplicateSound(message){
                        showActionSheet(message: message, title: "Errore")
                        return //usciamo dal metodo (la parola esiste già)
                    }
                    catch let error{
                        showActionSheet(message: "errori nel salvataggio del suono", title: "Errore")
                        print(error)
                    }
                }
                
                try modelGlossaryImpl.insertWord(wordModel: word)
                
                //inserimento andato a buon fine:
                //cancelliamo il file audio temp
                try? fileManager.removeItem(at: urlAudioTemp!)
                //puliamo l'interfaccia:
                WordEditText.text! = ""
                means.removeAll()
                TableMeans.reloadData()
                
                //messaggio di successo:
                let alertController = UIAlertController(title: "",  message: "La parola è stata aggiunta",preferredStyle: .alert)
                
                self.present(alertController, animated: true, completion: nil)
                
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when){
                    // your code with delay
                    alertController.dismiss(animated: true, completion: nil)
                }
                
                //una volta che è stato inserita la parola, il bottone delete sound deve essere nero
                //il file temp sarà stato cancellato
                if(!fileManager.fileExists(atPath: self.urlAudioTemp!.path)){
                    self.DeleteButton.imageView!.tintColor = UIColor.black
                }

            } catch let error as NSError {
                showActionSheet(message: "la parola non è valida, controlla se esiste già..", title: "Errore")
                //se c'è stato un errore nel salvataggio e abbiamo già copiato il file in document lo cancelliamo
                if(urlSound != nil){
                    try? FileManager.default.removeItem(at: urlSound!)
                }
            }
          
         }

    }
    
    // MARK: - Navigation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Perform Segue
        performSegue(withIdentifier: SegueForAddMeans, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueForAddMeans {
            if let indexPath = TableMeans.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! PageViewControllerAddCategoryExample
                destinationViewController.indexRow = indexPath.row
                destinationViewController.examples = means[indexPath.row].examples
                destinationViewController.delegateWord = self
                destinationViewController.categories = means[indexPath.row].categories
                destinationViewController.word = self.WordEditText.text!
            }
        }
        
        if segue.identifier == seguePicker {
            let destinationViewController = segue.destination as! PickerViewController
            destinationViewController.pickerDataSource = tempListMeanAdvice
            destinationViewController.mytitle = "Scegli un significato:"
            destinationViewController.delegatePresentingController = self
        }
    }

    func addExampleForMean(example: String, indexMean: Int) {
        means[indexMean].examples.append(example)
    }
    
    func removeExampleForMean(indexMean: Int, indexExample: Int){
        means[indexMean].examples.remove(at: indexExample)
    }
    
    
    func addCategoryForMean(category: String, indexMean: Int) {
        means[indexMean].categories.append(category)
    }

    func removeCategoryForMean(indexMean: Int, indexCategory: Int){
        means[indexMean].categories.remove(at: indexCategory)
    }
    
    func showListAdviceMeans(adviceMeans: [String], error: Error?){
        self.buttonAdvice.isEnabled = true
        if(error != nil){
            showActionSheet(message: "", title: error!.localizedDescription)
        }
        else if(adviceMeans.count==0){
            showActionSheet(message: "nessun significato trovato", title: "")
        }
        else{
            //tutto ok
            tempListMeanAdvice = adviceMeans
            performSegue(withIdentifier: seguePicker, sender: self)
           
        }

    }
    
    func showActionSheet(message:String,title:String){
        let alertController = UIAlertController(title: title,  message: message,preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)

    }
    
    func setChoice(value: String) {
        let mean = MeanModel()
        mean.mean = value
        self.means.append(mean)
        self.TableMeans.beginUpdates()
        self.TableMeans.insertRows(at: [IndexPath(row: self.means.count-1, section: 0)], with: .automatic)
        self.TableMeans.endUpdates()
    }
    
    //http://stackoverflow.com/questions/39558868/check-internet-connection-ios-10
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }

    //for record and play:
    func inizializationForSound(){
        let fileManager = FileManager.default
        urlAudioTemp = fileManager.temporaryDirectory.appendingPathComponent("temp")
        if(fileManager.fileExists(atPath: urlAudioTemp!.path)){
            try? fileManager.removeItem(at: urlAudioTemp!)
        }
        
        let imageViewPlayBtn = UIImageView(image:PlayButton.currentImage!.withRenderingMode(.alwaysTemplate))
        PlayButton.setImage(imageViewPlayBtn.image!, for: .normal)
        
        let imageViewRecordBtn = UIImageView(image:RecordButton.currentImage!.withRenderingMode(.alwaysTemplate))
        RecordButton.setImage(imageViewRecordBtn.image!, for: .normal)
        
        let imageViewDeleteBtn = UIImageView(image:DeleteButton.currentImage!.withRenderingMode(.alwaysTemplate))
        DeleteButton.setImage(imageViewDeleteBtn.image!, for: .normal)
    }
    
    func onRecord() {
        self.RecordButton.imageView!.tintColor = UIColor.green
    }
    
    func onPlay() {
        self.PlayButton.imageView!.tintColor = UIColor.green
    }
    func onStopPlay() {
        self.PlayButton.imageView!.tintColor = UIColor.black
    }
    func onErrors(error: String) {
        showActionSheet(message: error, title: "")
    }
    
    func onStopRecord() {
        self.RecordButton.imageView!.tintColor = UIColor.black
        if(FileManager.default.fileExists(atPath: urlAudioTemp!.path)){//se la registrazione è andata a buon fine, esiste il file audio:
            self.DeleteButton.imageView!.tintColor = UIColor.orange
        }
    }
    
    
    
    @IBAction func record(_ sender: Any)
    {
        do{
            try audioManager!.record(seconds: 15.0)
        }catch let error{
            showActionSheet(message: "errori per la registrazione", title: "")
            self.RecordButton.imageView!.tintColor = UIColor.black
            print(error)
        }
    }
    
    @IBAction func play(_ sender: Any) {
        do{
            try audioManager!.play()
        }catch let error{
            showActionSheet(message: "errori playing", title: "")
            self.PlayButton.imageView!.tintColor = UIColor.black
            print(error)
        }
    }

    @IBAction func deleteRecord(_ sender: Any) {
        if(!audioManager!.isPlaying() && !audioManager!.isRecording()){
            let fileManager = FileManager.default
            if(fileManager.fileExists(atPath: urlAudioTemp!.path)){
                try? fileManager.removeItem(at: urlAudioTemp!)
                self.DeleteButton.imageView!.tintColor = UIColor.black
            }
        }
    }
    
    enum SoundError: Error {
        case duplicateSound(message: String)
    }
    
    /*override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
        super.touchesEnded(touches, with: event)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                      with event: UIEvent?){
        self.view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }*/
    //dismiss keyboard:
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    /*//Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        UITapGestureRecognizer.
        
    }*/
}
