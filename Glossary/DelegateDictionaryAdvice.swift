//
//  DelegateDictionaryAdvice.swift
//  Glossary
//
//  Created by Alessandro Novi on 22/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

@objc protocol DelegateDictionaryAdvice{
    @objc optional func showListAdviceMeans(adviceMeans: [String], error: Error?)
    @objc optional func showListAdviceExample(adviceExample: [String], error: Error?)
}
