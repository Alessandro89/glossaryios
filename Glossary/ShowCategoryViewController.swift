//
//  CategoryTableViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 07/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class ShowCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var TableCategory: UITableView!

    
    let cellId = "idCell"
    var categories = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableCategory.delegate = self
        self.TableCategory.dataSource = self
        TableCategory.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: cellId)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = categories[indexPath.row]
        
        let row = indexPath.row
        cell.textLabel?.font = cell.textLabel?.font.withSize(15.0)
        cell.textLabel?.textColor = UIColor.darkGray
        return cell
    }

}
