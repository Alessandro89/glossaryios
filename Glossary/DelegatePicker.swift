//
//  delegatePicker.swift
//  Glossary
//
//  Created by Alessandro Novi on 08/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

protocol DelegatePicker{
    func setChoice(value: String)
}
