//
//  DelegatePlayAudio.swift
//  Glossary
//
//  Created by Alessandro Novi on 21/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

protocol DelegatePlayAudioCustomRow{
    func errorsPlayRow(error:String)
    func saveAudioManagerForPlaying(currentPlaying:ManagerAudio)
    func removeAudioManagerForStopPlaying(currentStopPlaying:ManagerAudio)



}
