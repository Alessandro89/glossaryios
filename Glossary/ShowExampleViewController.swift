//
//  ShowExampleTableViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 01/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class ShowExampleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {


    @IBOutlet weak var TableExample: UITableView!

    let cellId = "simpleCell"
    var listExample = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableExample.delegate = self
        self.TableExample.dataSource = self
        TableExample.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: cellId)
    }


    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listExample.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = listExample[indexPath.row]
        cell.textLabel?.font = cell.textLabel?.font.withSize(13.0)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 3
        return cell
    }
    

}
