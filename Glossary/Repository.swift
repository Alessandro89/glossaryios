//
//  Repository.swift
//  Glossary
//
//  Created by Alessandro Novi on 26/01/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class Repository {
    //lo faccio statico, così rimane per tutte le viste
    static var listWord = [WordModel]()
    //static list categories (le categorie che ci sono già nel repository
    static var categories:[String] = ["tecnologia","oggetti","natura"]
}
