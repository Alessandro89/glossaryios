//
//  SearchWordCategoryViewController.swift
//  Glossary
//
//  Created by Alessandro Novi on 08/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class SearchWordCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DelegatePlayAudioCustomRow {
    
    var selectedCategoryIndex = [IndexPath]()
    var modelGlossaryImpl = ModelGlossaryImpl()



    @IBOutlet weak var TableCategory: UITableView!
    
    @IBOutlet weak var labelResults: UILabel!
    
    @IBOutlet weak var TableWordsFound: UITableView!
    
    var SegueShowMeans = "showMeans"

    var categories = [String]()
    var categoriesSelected = [String]()
    var words = [WordModel]()
    var managerAudios = [ManagerAudio]()

    let textCellIdentifier = "simpleCellAudio"

    override func viewDidLoad() {
        super.viewDidLoad()
        TableCategory.delegate = self
        TableCategory.dataSource = self
        TableWordsFound.delegate = self
        TableWordsFound.dataSource = self
        //TableWordsFound.isHidden = true
        TableCategory.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: textCellIdentifier)
        
        self.labelResults.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categories = modelGlossaryImpl.getAllCategories()
        self.TableCategory.reloadData()
        searchWordForCategoriesAndUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView.isEqual(TableCategory)){
            return categories.count
        }
        else{
            return words.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if(tableView.isEqual(TableCategory)){
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath);
            let row = indexPath.row
            cell.textLabel?.text = categories[row]
            cell.textLabel?.font = cell.textLabel?.font.withSize(15.0)
            cell.textLabel?.textColor = UIColor.darkGray
            if(selectedCategoryIndex.contains(indexPath)){
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }

            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! SimpleTableCellAudio
            cell.labelWord.text = words[indexPath.row].word
            cell.labelWord?.font = cell.labelWord?.font.withSize(15.0)
            cell.labelWord?.textColor = UIColor.darkGray
            cell.idRow = indexPath.row
            cell.delegatePlayWord = self
            cell.playButton.isHidden = true
            cell.playButton.imageView!.tintColor = UIColor.black
            if(words[indexPath.row].pathSoundBookMark != nil){
                var isStale = false
                try? cell.urlSound  = URL(resolvingBookmarkData: words[indexPath.row].pathSoundBookMark!, bookmarkDataIsStale: &isStale)
            }
            cell.accessoryType = .disclosureIndicator
            print(managerAudios.count)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         if(tableView.isEqual(TableCategory)){
            let categoryStr = tableView.cellForRow(at: indexPath)?.textLabel?.text

            if(tableView.cellForRow(at: indexPath)?.accessoryType  == .checkmark){
                tableView.cellForRow(at: indexPath)?.accessoryType = .none
                selectedCategoryIndex.remove(at: selectedCategoryIndex.index(of: indexPath)!)
                
                if let indexCategory = categoriesSelected.index(of: categoryStr!){
                    categoriesSelected.remove(at: indexCategory)
                }
                
            }
            else{
                tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
                categoriesSelected.append(categoryStr!)
                selectedCategoryIndex.append(indexPath)

            }
            
            words.removeAll()
            searchWordForCategoriesAndUpdate()
            
        }
        
        else{
            performSegue(withIdentifier: SegueShowMeans, sender: self)
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func searchWordForCategoriesAndUpdate(){
        /*for word in Repository.listWord
        {
            for means in word.means{
                for category in categoriesSelected{
                    if means.categories.contains(category){
                        //se la parola contiene una delle categorie l'aggiungiamo
                        if(!words.contains(where: { (thisWord:WordModel) -> Bool in
                            return thisWord.word == word.word}))
                        { //se no aggiungi più di una parola..o facciamo un set?!uhm
                            words.append(word)
                        }
                        else{
                            continue
                        }
                    }
                }
            }
        }*/
        if(categoriesSelected.count>0){
            words = self.modelGlossaryImpl.getWordsByCategories(categories: categoriesSelected)
        }
        if(words.count>0){
            self.labelResults.isHidden = false
            //self.TableWordsFound.isHidden = false
            self.TableWordsFound.separatorStyle = .singleLine
            
            deletesManagersAudioNotPlaying()
        }
        else{
            self.labelResults.isHidden = true
            //così sembra che non ci sia.. (per l'autolayout)
            self.TableWordsFound.separatorStyle = .none

            //self.TableWordsFound.isHidden = true
        }
        
        TableWordsFound.reloadData()
    }
    
    func deletesManagersAudioNotPlaying(){
        var indexesElementsNotPlaying = [Int]()
        //qualche volta non riesce a eliminare il file audio andato in stop ..
        for managerAudio in managerAudios{
            if(!managerAudio.isPlaying()){
                if let index = managerAudios.index(of: managerAudio){
                    indexesElementsNotPlaying.append(index)
                }
            }
        }
        
        for index in indexesElementsNotPlaying{
            managerAudios.remove(at: index)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueShowMeans {
            if let indexPath = TableWordsFound.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! ShowMeanTableViewController
                destinationViewController.listMean = self.words[indexPath.row].means
                destinationViewController.word = self.words[indexPath.row].word
                
            }
        }
    }

    func errorsPlayRow(error: String) {
        showActionSheet(message: "error playing", title: "")
    }
    
    func showActionSheet(message:String,title:String){
        let alertController = UIAlertController(title: title,  message: message,preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func saveAudioManagerForPlaying(currentPlaying: ManagerAudio) {
        self.managerAudios.append(currentPlaying)
    }
    
    func removeAudioManagerForStopPlaying(currentStopPlaying: ManagerAudio) {
        if let indexAudio = self.managerAudios.index(of: currentStopPlaying){
            self.managerAudios.remove(at: indexAudio)
        }
    }

}
