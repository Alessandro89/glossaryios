//
//  Word.swift
//  Glossary
//
//  Created by Alessandro Novi on 25/01/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class WordModel{
    var word:String = ""
    var means = [MeanModel]()
    var dateOfWord:NSDate = NSDate()
    var pathSoundBookMark: Data?
    
    /*init(word: String, means: [MeanModel]) {
        self.word = word
        self.means = means
    }*/
    
    init(word: String, means: [MeanModel], pathSoundBookMark: Data?) {
        self.word = word
        self.means = means
        self.pathSoundBookMark = pathSoundBookMark
    }
}
