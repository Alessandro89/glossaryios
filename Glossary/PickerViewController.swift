//
//  ViewController.swift
//  tryPicker
//
//  Created by Alessandro Novi on 06/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class PickerViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate{

    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var delegatePresentingController:DelegatePicker?

    var value = ""
    var mytitle: String? = ""
    var height = 70 as CGFloat
    var pickerDataSource = [String]();
    
    override func viewDidLoad() {
        self.labelTitle.text = mytitle
        super.viewDidLoad()
        self.pickerView.dataSource = self;
        self.pickerView.delegate = self;
        value = pickerDataSource[0]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    numberOfRowsInComponent component: Int) -> Int{
        return pickerDataSource.count;
    }

    
    /*func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String?{
        let label = UILabel(frame: CGRectMake(0, 0, 400, 44));
        label.lineBreakMode = .byWordWrapping;
        label.numberOfLines = 0;
        label.text = pickerDataSource[row]
        label.sizeToFit()
        return label;
        //return pickerDataSource[row]

    }*/
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let frameRect = CGRect(x: 0, y: 0, width: 250, height: self.height)
        let label = UILabel(frame: frameRect )
        label.lineBreakMode = .byWordWrapping;
        label.numberOfLines = 0
        label.font!.withSize(12)
        ///label.text = pickerDataSource[row]
        label.text = pickerDataSource[row]
        //label.sizeToFit()
        
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView,rowHeightForComponent component: Int) -> CGFloat{
        return height
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int){
        value = pickerDataSource[row]
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        delegatePresentingController!.setChoice(value: value)
        self.dismiss(animated: true, completion: nil)
    }
   
    

}

