//
//  DictionaryAdvice.swift
//  Glossary
//
//  Created by Alessandro Novi on 22/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class DictionaryAdvice{
    //: NSObject, URLSessionTaskDelegate
    var urlforTranslation = "https://glosbe.com/gapi/"
    var urlforExample =  "https://glosbe.com/gapi/"
    var delegateDictionary:DelegateDictionaryAdvice

    init(word: String, delegateDictionary: DelegateDictionaryAdvice) {
        urlforTranslation += "translate?from=eng&dest=it&format=json&pretty=true&phrase="+word
        urlforExample += "tm?from=eng&dest=it&format=json&phrase="+word
        self.delegateDictionary = delegateDictionary
    }
    
    func getlistMeans(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        var arrayMeaningsOfWordStr = [String]()
        //let session = URLSession.shared
        let urlconfig = URLSessionConfiguration.default
        urlconfig.timeoutIntervalForRequest = 10
        urlconfig.timeoutIntervalForResource = 15

        //let session = URLSession(configuration: urlconfig, delegate: self, delegateQueue: nil)
        let session = URLSession(configuration: urlconfig, delegate: nil, delegateQueue: nil)
        let urlConnection = URL(string: urlforTranslation)
        let urlRequest = URLRequest(url: urlConnection!)
        let sessionDataTask = session.dataTask(with: urlRequest) { (data:Data?, response:URLResponse?, error:Error?) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }

            
            if(error != nil){
                print(error!.localizedDescription)
                //eccezione..
            }
            if(response != nil){
                print(response!.description)
                
            }
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
            }

            if(data != nil){
                //ora dobbiamo prenderci i vari dati da data a json:
               
                    if let json = try?JSONSerialization.jsonObject(with: data!, options: []){ //nil se riscontra un errore("if false")
                        if let dictionary = json as? [String: Any] { //nil se non riesce a convertire("if false")
                            if let arraytuc = dictionary["tuc"] as? [Any]{
                                for objectTuc in arraytuc {
                                    if let dictionaryTuc = objectTuc as? [String:Any]{
                                        if let arraymeaning = dictionaryTuc["meanings"] as? [Any]{
                                            for meaning in arraymeaning {
                                                if let dictionaryMeaning = meaning as? [String:Any]{
                                                    if let meaningStr = dictionaryMeaning["text"] as? String{
                                                        arrayMeaningsOfWordStr.append(meaningStr)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.delegateDictionary.showListAdviceMeans!(adviceMeans: arrayMeaningsOfWordStr, error: error)
                    }
            }
        }
        
        sessionDataTask.resume()
    }
    
    func getlistExample(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        var arrayExampleOfWordStr = [String]()

        let urlconfig = URLSessionConfiguration.default
        urlconfig.timeoutIntervalForRequest = 10
        urlconfig.timeoutIntervalForResource = 15
        let session = URLSession(configuration: urlconfig, delegate: nil, delegateQueue: nil)
        let urlConnectionExample = URL(string: urlforExample)
        let urlRequestExample = URLRequest(url: urlConnectionExample!)

        let sessionDataTaskForExample = session.dataTask(with: urlRequestExample){  (data:Data?, response:URLResponse?, error:Error?) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            if(error != nil){
                print(error!.localizedDescription)
            }
            if(response != nil){
                print(response!.description)
            }
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
            }
            if(data != nil){
                if let json = try?JSONSerialization.jsonObject(with: data!, options: []){
                    if let dictionary = json as? [String: Any] {
                        if let arrayexamples = dictionary["examples"] as? [Any]{
                            for example in arrayexamples {
                                if let dictionaryExample = example as? [String:Any]{
                                    if let firstExample = dictionaryExample["first"] as? String{
                                        arrayExampleOfWordStr.append(firstExample)
                                    }
                                    if let secondExample = dictionaryExample["second"] as? String{
                                        arrayExampleOfWordStr.append(secondExample)
                                    }
                                }
                            
                            
                            }
                        
                        }
                    
                    }
                }
            }
            DispatchQueue.main.async {
                self.delegateDictionary.showListAdviceExample!(adviceExample: arrayExampleOfWordStr, error: error)
            }
         
        }
        sessionDataTaskForExample.resume()
    }
    
    /*func urlSession(_ session: URLSession,task: URLSessionTask,didCompleteWithError error: Error?)
    {
        print(error!)
        print(task.description)
    }*/
   
}

