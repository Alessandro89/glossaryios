//
//  PageViewControllerSearch.swift
//  Glossary
//
//  Created by Alessandro Novi on 08/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

class PageViewControllerSearch: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var pages = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        let searchWord:UIViewController! = storyboard?.instantiateViewController(withIdentifier: "searchWord")
        let searchWordCategory:UIViewController!  = storyboard?.instantiateViewController(withIdentifier: "searchWordForCategory")
        
        
        pages.append(searchWord)
        pages.append(searchWordCategory)
        
        setViewControllers([searchWord], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.navigationItem.title = "Glossary"
        
        self.view.backgroundColor = UIColor.white
        
    
        //page control
        UIPageControl.appearance().backgroundColor = UIColor.init(red: 90/255, green: 160/255, blue: 220/255, alpha: 0.7)
        
        //eng-it
        let rightButton = UIBarButtonItem(title: "eng-it", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        navigationController?.navigationBar.topItem?.rightBarButtonItem = rightButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //https://developer.apple.com/reference/uikit/uipageviewcontrollerdelegate
        
        //UIPageViewControllerSpineLocation.max
        let currentIndex = pages.index(of: viewController)!
        let previousIndex = abs((currentIndex - 1) % pages.count)
        
        return pages[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    /*func pageViewController(_ pageViewController: UIPageViewController,
                            spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation{
        
        if(orientation == UIInterfaceOrientation.unknown){
            return UIPageViewControllerSpineLocation.max
        }
        else{
            return UIPageViewControllerSpineLocation.min
        }
        
    }*/

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
