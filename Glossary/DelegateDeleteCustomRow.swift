//
//  deleteCustomRow.swift
//  Glossary
//
//  Created by Alessandro Novi on 04/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit

protocol DelegateDeleteCustomRow {
    func deleteRow(idrow:Int)
}
