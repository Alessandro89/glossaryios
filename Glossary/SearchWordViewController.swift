//
//  SearchWordController.swift
//  Glossary
//
//  Created by Alessandro Novi on 08/02/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import AVFoundation

class SearchWordViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,DelegateListenerAudio, UITextFieldDelegate{

    @IBOutlet weak var TableMeans: UITableView!
    
    @IBOutlet weak var textFieldSearchWord: UITextField!
    
    @IBOutlet weak var PlayButton: UIButton!
    @IBOutlet weak var labelWordFound: UILabel!
    
    var SegueShowCategoryExample = "ShowCategoryExample"
    var modelGlossaryImpl = ModelGlossaryImpl()
    var audioManager:ManagerAudio?
    var urlSound:URL?
    
    @IBOutlet weak var labelResults: UILabel!
    var means = [MeanModel]()
    let textCellIdentifier = "TextCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        initializationForPlaySound()
       
        TableMeans.delegate = self
        TableMeans.dataSource = self
        //TableMeans.isHidden = true
        TableMeans.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: textCellIdentifier)
        
        self.labelResults.isHidden = true
        
        textFieldSearchWord.delegate = self

    }
    
    func initializationForPlaySound(){
        self.PlayButton.isHidden = true
        let imageViewPlayBtn = UIImageView(image:PlayButton.currentImage!.withRenderingMode(.alwaysTemplate))
        PlayButton.setImage(imageViewPlayBtn.image!, for: .normal)
        audioManager = ManagerAudio(isRecord: false, urlAudio: nil, delegateListener: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return means.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath);
        let row = indexPath.row
        cell.textLabel?.text = means[row].mean
        cell.textLabel?.font = cell.textLabel?.font.withSize(14.0)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 3
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
   
    @IBAction func searchWord(_ sender: UIButton) {
        var found = false
        if(!textFieldSearchWord.text!.isEmpty){
            if let word = modelGlossaryImpl.getWord(name: textFieldSearchWord.text!){
                self.labelResults.isHidden = false
                self.TableMeans.isHidden = false
                labelWordFound.text = word.word
                labelWordFound.sizeToFit()
                self.means = word.means
                TableMeans.reloadData()
                found = true
                //pathSound = word.pathSound
                if(word.pathSoundBookMark != nil){
                    
                    var isStale = false
                    try? self.urlSound = URL(resolvingBookmarkData: word.pathSoundBookMark!, bookmarkDataIsStale: &isStale)
                    self.PlayButton.isHidden = false
                    self.audioManager?.urlAudio = urlSound!
                }
                else{
                    self.urlSound = nil
                    self.PlayButton.isHidden = true
                    self.audioManager?.urlAudio = nil
                }
                
            }
        }
        if(!found){
            clearSearch()
            
            let alertController = UIAlertController(title: "",  message: "La parola non è stata trovata",preferredStyle: .alert)
            self.present(alertController, animated: true, completion: nil)
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                // your code with delay
                alertController.dismiss(animated: true, completion: nil)
            }
            self.urlSound = nil
            self.PlayButton.isHidden = true
            self.audioManager?.urlAudio = nil
        }
    }
    
    func clearSearch(){
        self.labelResults.isHidden = true
        self.labelWordFound.text = ""
        //TableMeans.isHidden = true (for layout)
        means.removeAll()
        TableMeans.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Perform Segue
        performSegue(withIdentifier: SegueShowCategoryExample, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueShowCategoryExample {
            if let indexPath = TableMeans.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! PageViewControllerShowCategoryExample
                destinationViewController.categories = means[indexPath.row].categories
                destinationViewController.examples = means[indexPath.row].examples
            }
        }
    }
    @IBAction func playAction(_ sender: Any) {
        if(self.urlSound != nil){
            do{
                //for debugging:
                print(urlSound!.path)
                if(!FileManager.default.fileExists(atPath: urlSound!.path))
                {
                    print("il file non esiste !! :(")
                }
                //let path = try FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                //print(path[0]) //e' diverso eheh
                //var urlWord = path[0].appendingPathComponent(labelWordFound.text!)
                //audioManager!.urlAudio = urlWord
                //proviamo a togliere l'm4a: "audioFileName.m4a" -> "audioFileName", eheh
                //var newPath:String = "file://"
                //newPath.append(urlSound!.path)
                //print(newPath)
                
                /*if(FileManager.default.fileExists(atPath: newPath))
                {
                    print("hai sbagliato il salvataggio del path!!!!! :) ")
                }else{
                    print("il file non esiste !! :(")
                }*/
                try audioManager!.play()
            }catch let error{
                showActionSheet(message: "error playing", title: "")
                print(error)
            }
        }
    }
    func onPlay() {
        self.PlayButton.imageView!.tintColor = UIColor.green
    }
    func onStopPlay() {
        self.PlayButton.imageView!.tintColor = UIColor.black
    }
    
    func showActionSheet(message:String,title:String){
        let alertController = UIAlertController(title: title,  message: message,preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //dismiss keyboard:
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}
