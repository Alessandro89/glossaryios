//
//  ManagerAudio.swift
//  Glossary
//
//  Created by Alessandro Novi on 21/03/17.
//  Copyright © 2017 Alessandro Novi. All rights reserved.
//

import UIKit
import AVFoundation

class ManagerAudio: NSObject, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    var audioRec:AVAudioRecorder?
    var audioSession:AVAudioSession?
    var sound:AVAudioPlayer?
    var urlAudio:URL?
    var delegateListener:DelegateListenerAudio
    init(isRecord: Bool, urlAudio: URL?, delegateListener: DelegateListenerAudio){
        self.delegateListener = delegateListener
        self.urlAudio = urlAudio
        
        audioSession = AVAudioSession.sharedInstance()
        try? audioSession?.setActive(false)
        if(isRecord){
            try? audioSession!.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        }
        else{
            try? audioSession!.setCategory(AVAudioSessionCategoryPlayback)
        }
    }
    
    func record(seconds: Double) throws{
        if(audioRec != nil && audioRec!.isRecording){
            stopRecord()
        }
        else{
           
            try audioSession!.setActive(true)
            let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
            ]
            askRecordPermission()
            audioRec = try AVAudioRecorder(url: urlAudio!, settings: settings)
            audioRec!.delegate = self
            audioRec!.record(forDuration: seconds)
            delegateListener.onRecord?()
        }
    }
    
    func isRecording()->Bool{
        if( audioRec != nil && audioRec!.isRecording){
            return true
        }
        else{
            return false
        }
    }
    
    func isPlaying()->Bool{
        if( sound != nil && sound!.isPlaying){
            return true
        }
        else{
            return false
        }
    }
    
    
    func askRecordPermission(){
    /*
     https://developer.apple.com/reference/avfoundation/avaudiosession/1616601-requestrecordpermission
     recordingSession.requestRecordPermission() { [unowned self] allowed in
     DispatchQueue.main.async {
     if allowed {
     self.loadRecordingUI()
     } else {
     // failed to record!
     }
     }
     }*/
    }
    func stopRecord(){
        if(audioRec!.isRecording){
            audioRec!.stop()
        }
        try? audioSession!.setActive(false)
        delegateListener.onStopRecord?()
    }
    
    func play() throws {
        if(audioRec == nil || !(audioRec!.isRecording)){ //se non stai registrando, allora interagisci con il playbutton
            if(sound != nil && sound!.isPlaying){
                stopPlay()
            }
            else{
                try audioSession!.setActive(true)
                sound = try AVAudioPlayer(contentsOf: urlAudio!)
                sound!.delegate = self
                sound!.play()
                delegateListener.onPlay?()
            }
        }
    }
    
    func stopPlay(){
        if(sound!.isPlaying){
            sound!.stop()
        }
        try? audioSession!.setActive(false)
        delegateListener.onStopPlay?()
    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        stopRecord()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        stopPlay()
    }
}
